# 1 - Instalar o python, dentro da pasta 'assets':
-  Marcar a opção para adicionar o python na variável de ambiente PATH

# 2 - Descompactar o CMDER, dentro da pasta 'assets':
-  O CMDER é um terminal mais performático e agradável de usar do que o nativo do Windows (CMD)
 
# 3 - Instalar as dependências do projeto dentro da pasta begraf:
-  Executar o comando como administrador: "pip install -r requirements.txt"

# 4 - Se der um erro ("Microsoft Visual C++ 14.0 is required. Get it with "Microsoft Visual C++ Build Tools") em 3:
- Instalar "vs_buildtools.exe", dentro da pasta 'assets'
- Ver imagem "dependencias do windows c++.PNG", dentro da pasta 'assets'
- Add variáveis de ambiente no PATH (talvez não seja necessário)
consultar imagem "variáveis de ambiente da etapa 4.PNG", dentro da pasta 'assets'

# 5 - Executar os comandos
- scrapy crawl [nome do scrapy] -o [nome do arquivo de saida].csv -t csv
- scrapy crawl [nome do scrapy] -o [nome do arquivo de saida].json -t json

## Exemplo
- scrapy crawl 360imprimir -o tabela-360imprimir.csv -t csv
- scrapy crawl zapgrafica -o zapgrafica-dados.json -t json