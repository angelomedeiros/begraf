# -*- coding: utf-8 -*-
import scrapy
import re
import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.select import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

if __name__ != "__main__":
    from begraf.items import Product

_PRODUCT_RE = re.compile(r"^(https://www.printi.com.br/)(.+)")


class PrintSpider(scrapy.Spider):
    name = 'print'
    allowed_domains = ['printi.com.br']
    start_urls = ['http://printi.com.br/']
    CHROMEDRIVER_PATH = "/home/angelo/drivers/chromedriver"

    def __init__(self):
        self.setUpWebdriver()
        # self.printUrls()
        self.getProductDetails()

    def setUpWebdriver(self):
        self.options = Options()
        # self.options.add_argument("--window-size=1920,1080")
        # self.options.add_argument("--start-maximized")
        # self.options.add_argument("--headless")

        self.driver = webdriver.Chrome(
            options=self.options,
            executable_path=self.CHROMEDRIVER_PATH)
        self.driver.maximize_window()
        # self.driver.get(self.start_urls[0])

    def getElementBySelectorWithWait(self, css_selector):
        return WebDriverWait(self.driver, 30).until(
            EC.presence_of_element_located(
                (By.CSS_SELECTOR, css_selector))
        )

    def getElementVisibleBySelectorWithWait(self, element):
        return WebDriverWait(self.driver, 30).until(
            EC.visibility_of(element)
        )

    def hoverElement(self, css_selector):
        hover_element = self.driver.find_element_by_css_selector(css_selector)
        hoverover = ActionChains(
            self.driver).move_to_element(hover_element).perform()

    def getLinksProducts(self):
        menu = self.getElementBySelectorWithWait(
            ".atm-header-button.atm-header-button-products")

        menu.click()

        self.getElementBySelectorWithWait(
            ".mol-products-menu-menu .atm-products-menu-item")

        menu_categories = self.getElementBySelectorWithWait(
            ".mol-products-menu-menu")

        categories = menu_categories.find_elements_by_css_selector("li")

        product_urls_list = []

        for category in categories:
            category = category.click()
            products = self.driver.find_elements_by_css_selector(
                ".mol-products-menu-content li a")
            for product in products:
                href = _PRODUCT_RE.match(product.get_attribute("href"))
                url = href.group(1) + "configuracao-" + href.group(2)
                product_urls_list.append(url)

        return {*product_urls_list}

    def getOptionConfiguration(self, nth_child=1):
        return self.getElementBySelectorWithWait(
            f".app__config__options-listing > div > ul > li:nth-child({nth_child})")

    def verifyAndClick(self, element):
        while True:
            if element.get_attribute("class") == "app__input__radio app__input__radio--checked":
                break
            element.click()
            time.sleep(0.05)

    def getProductDetails(self):
        url = "https://www.printi.com.br/configuracao-adesivo"
        self.driver.implicitly_wait(10)
        self.driver.get(url)
        configureElement = self.getElementBySelectorWithWait(
            ".app__config__creation-upload")
        configureElement.click()

        self.getElementBySelectorWithWait(
            ".mol-btn-group button[value=list]").click()

        firstOptionConfiguration = self.getOptionConfiguration()

        elements = firstOptionConfiguration.find_elements_by_css_selector(
            "ul li")

        for e in elements[:-1]:
            t = e.find_element_by_css_selector(
                ".app__input__radio")

            self.verifyAndClick(t)

            child_2 = self.getOptionConfiguration(2)
            for e2 in child_2:
                t2 = e2.find_element_by_css_selector(
                    ".app__input__radio")
                self.verifyAndClick(e2)

    def printUrls(self):
        print(self.getLinksProducts())

    def parse(self, response):
        pass


if __name__ == "__main__":
    PrintSpider()
