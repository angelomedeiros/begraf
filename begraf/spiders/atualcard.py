import re
import json
import copy
from collections import OrderedDict
from urllib.parse import urljoin

from scrapy import Request
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.http import FormRequest

_API_FILTER_ENDPOINT = "/produto/filtrar"
_API_SEARCH_ENDPOINT = "/produto/buscar-produtos"

_API_TAG = {
    '1': "papel",
    '2': "cor",
    '3': "acabamento",
    '4': "tamanho",
    '7': "páginas",
    '9': "acabamento-incluso",
    '6': "quantidade"
}
_ATTRIBUTE_FROM_API_TAG = {
    'papel': "material",
    'cor': "color",
    'acabamento': "finish",
    'tamanho': "size",
    'páginas': "pages",
    'acabamento-incluso': "aditional-finish",
    'quantidade': "quantity"
}

_PRODUCT_URL_RE = re.compile(
    r"^https:/(?:/[^/]+){2}/(?P<pkMenu>\d+)/(?P<menu>\d+)(?:/(?P<submenu>\d+))?(?:/|$)"
)


def _product_url(url):
    matched = _PRODUCT_URL_RE.match(url)
    if matched:
        return matched[0].rstrip('/')


class AtualCardSpider(CrawlSpider):
    name = "atualcard"
    allowed_domains = ("atualcard.com.br",)
    start_urls = ("https://www.atualcard.com.br",)
    rules = (
        # Produtos
        Rule(
            LinkExtractor(
                allow=r"atualcard\.com\.br/[a-z][^/]*(?:/\d+){2,3}",
                restrict_css="div.box-menu a.nav-prod__link",
                process_value=_product_url),
            callback="handle_product"),

        # Categorias presentes entre os produtos
        Rule(
            LinkExtractor(
                allow=r"atualcard\.com\.br/[a-z][^/]*/\d+$",
                restrict_css="div.box-menu a.nav-prod__link")))

    def handle_product(self, res):
        p_id = _PRODUCT_URL_RE.match(res.url).groupdict()
        p_id = {k: v for k, v in p_id.items() if v is not None}

        cb = self.decompose_product
        if len(p_id) == 3:
            cb = self.handle_metaproduct
            del p_id['submenu']

        req = FormRequest(
            url=res.urljoin(_API_FILTER_ENDPOINT),
            formdata=p_id,
            callback=cb)
        req.meta['product_id'] = p_id
        yield req

    def handle_metaproduct(self, res):
        p_id = res.meta['product_id']

        payload = json.loads(res.text)
        st_codes = (st['pk_menu']
                    for st in payload['records']['subtipo']['menus'])

        for code in st_codes:
            form = copy.copy(p_id)
            form['submenu'] = code

            req = FormRequest(
                url=res.urljoin(_API_FILTER_ENDPOINT),
                formdata=form,
                callback=self.decompose_product,
                dont_filter=True)
            req.meta['product_id'] = form
            yield req

    def decompose_product(self, res):
        payload = json.loads(res.text)

        # ID do produto (pkMenu, menu, submenu)
        p_id = res.meta['product_id']

        # Atributos do produto
        attrs = payload['records']['atributos']

        # Configuração atual dos atributos
        current_config = res.meta.get('current_config', OrderedDict())
        if not current_config:
            current_config['menu'] = p_id['menu']

            if 'submenu' in p_id:
                current_config['submenu'] = p_id['submenu']

            for attr in attrs:
                k = attr['pk_atributo']
                v = attr['pk_dependencia_atributo']
                current_config[_API_TAG[k]] = v

        # Semelhante a current_config, mas guarda o nome dos atributos
        p_attrs = res.meta.get('product_attributes', OrderedDict())

        # Profundidade atual da recursão
        recursion_depth = res.meta.get('recursion_depth', 0)

        if recursion_depth < len(attrs):
            # Atributo a ser manipulado na profundidade atual
            attr = attrs[recursion_depth]

            # Tag do atributo, como entendida pela API
            attr_tag = _API_TAG[attr['pk_atributo']]

            for value in attr['dependencias']:
                # 1. Criamos o form com a configuração atual dos filtros
                form = copy.copy(p_id)
                form[attr_tag] = value['pk_dependencia_atributo']
                form['backup[]'] = current_config.values()

                # 2. Guardamos o nome do atributo
                p_attrs[attr_tag] = value['texto_dependencia_atributo']

                # 3. Enviamos a requisição para obter a nova
                # configuração dos filtros
                req = FormRequest(
                    url=res.urljoin(_API_FILTER_ENDPOINT),
                    formdata=form,
                    callback=self.decompose_product)
                req.meta['product_id'] = p_id
                req.meta['current_config'] = current_config
                req.meta['product_attributes'] = p_attrs
                req.meta['recursion_depth'] = recursion_depth + 1
                yield req

                # 4. Atualizamos a configuração dos atributos
                current_config[attr_tag] = form[attr_tag]
        else:
            form = copy.copy(p_id)
            form.update(current_config)

            req = FormRequest(
                url=res.urljoin(_API_SEARCH_ENDPOINT),
                formdata=form,
                callback=self.parse_product)
            req.meta['product_id'] = p_id
            req.meta['product_attributes'] = p_attrs
            yield req

    def parse_product(self, res):
        payload = json.loads(res.text)

        if not payload['success']:
            # Algumas das combinações geradas self.decompose_product
            # são inválidas; a API acusará a invalidade da informação
            # com success == False.
            return None

        payload = payload['records']

        # Identificação do produto, de acordo com a API
        p_id = res.meta['product_id']

        # Atributos do produto
        p_attrs = res.meta['product_attributes']

        product = {}

        # URL do produto
        product['url'] = _product_url(payload, p_id, res.url)

        # Nome do produto
        product['name'] = payload['dadosMenu']['nomeProduto']

        # Atributos passados via product_attributes
        for tag in ('papel', 'cor', 'acabamento', 'páginas',
                    'acabamento-incluso'):
            product[_ATTRIBUTE_FROM_API_TAG[tag]] = p_attrs.get(tag)

        # Preço do produto
        product['price'] = _product_price(payload)

        # Quantidade do produto
        product['quantity'] = _product_quantity(payload)

        # Prazo para produção
        product['deadline'] = _product_deadline(payload)

        # Dimensões
        product['dimensions'] = _product_dimensions(payload, p_attrs)

        # Acabamentos opcionais
        product['optional-finishes'] = _product_optional_finishes(payload)

        # Serviços opcionais
        product['optional-services'] = _product_optional_services(payload)

        # Gabaritos
        product['templates'] = _product_templates(payload, res.url)

        req = Request(product['url'], self.parse_product_image)
        req.meta['product'] = product
        yield req

    def parse_product_image(self, res):
        image_url = res.css(".fotorama .fotorama__active > img::attr(href)")

        product = res.meta['product']
        product['image-url'] = image_url.get()
        yield product


def _product_deadline(payload):
    value = payload['dadosMenu']['qtd_dia_prazo_produto']
    return int(value)


def _product_price(payload):
    value = payload['dadosMenu']['vlr_produto']
    value = value[:-1]
    return f"BRL {value}"


def _product_url(payload, product_id, base_url):
    # A URL inclui os IDs pkMenu e menu...
    rel_url = f"/{product_id['pkMenu']}/{product_id['menu']}"

    # ...e, se existir, submenu.
    if 'submenu' in product_id:
        rel_url += f"/{product_id['submenu']}"

    # Por fim, a URL contém o identificador gerado pela API.
    rel_url += f"/{payload['dadosMenu']['url_amigavel']}"

    return urljoin(base_url, rel_url)


def _product_quantity(payload):
    data = payload['dadosMenu']
    return {
        # Tamanho do lote
        'batch-size': int(data['unidades_produto']),

        # Quantidade mínima para o pedido ser válido
        'minimum-order': int(data['qtd_minima_venda_produto']),

        # Quantidade máxima para o pedido ser válido
        'maximum-order': int(data['qtd_maxima_venda_produto'])
    }


def _product_dimensions(payload, attrs):
    data = payload['dadosMenu']

    try:
        return {
            # Tag indicando o tamanho do produto
            'size': attrs['tamanho'],

            # Larguras mínima e máxima
            'width': {
                'minimum': float(data['largura_min_produto']),
                'maximum': float(data['largura_max_produto'])
            },

            # Alturas mínima e máxima
            'height': {
                'minimum': float(data['altura_min_produto']),
                'maximum': float(data['altura_max_produto'])
            }
        }
    except:
        return None


def _product_optional_finishes(payload):
    result = []
    if 'acabamentosOpcionais' in payload:
        for finish in payload['acabamentosOpcionais']:
            result.append({
                'name': finish['titulo'],
                'price': finish['valor'],
                'impact-on-deadline': finish['tempo']
            })
    return result


def _product_optional_services(payload):
    result = []
    if 'servicosOpcionais' in payload:
        for service in payload['servicosOpcionais']:
            result.append({
                'name': service['titulo'],
                'price': service['valor']
            })
    return result


def _product_templates(payload, base_url):
    result = []
    if 'gabaritos' in payload:
        for template in payload['gabaritos']:
            result.append({
                'application': template['nome'],
                'file-url': urljoin(base_url, template['dados'][0]['link'])
            })
    return result
