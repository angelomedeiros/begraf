# -*- coding: utf-8 -*-
import scrapy
import os
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.select import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from begraf.items import Product


class ZapgraficaSpider(scrapy.Spider):
    name = 'zapgrafica'
    allowed_domains = ['zapgrafica.com.br']
    start_urls = ['http://zapgrafica.com.br/']
    CHROMEDRIVER_PATH = os.path.join(os.path.dirname(__file__), "..","..","driver","chromedriver.exe")

    def __init__(self):
        self.setUpWebdriver()
        self.getLinksCategories()
        self.signIn()

    def setUpWebdriver(self):
        self.options = Options()
        self.options.add_argument("--window-size=1920,1080")
        self.options.add_argument("--start-maximized")
        self.options.add_argument("--headless")
        self.driver = webdriver.Chrome(
            options=self.options,
            executable_path=self.CHROMEDRIVER_PATH)
        self.driver.maximize_window()
        self.driver.get(self.start_urls[0])

    def signIn(self):
        inputEmail = self.driver.find_element_by_id("input-home1-email")
        inputEmail.send_keys("angelomedeiros3@gmail.com")
        inputPassword = self.driver.find_element_by_id("input-home1-senha")
        inputPassword.send_keys("1wdv4esz")
        btnEntrar = self.driver.find_element_by_id("btnEntrarHome").click()

    def getLinksCategories(self):
        menu = self.driver.find_element_by_css_selector(
            "#produtos a:first-child")
        menu.click()

        menuItems = self.driver.find_elements_by_css_selector("a.subMenuItem")
        links = {menuItem.get_attribute("href") for menuItem in menuItems}

        self.linksCategories = links

    def getDetails(self, productElement):
        boxInfoServicoElement = productElement.find_elements_by_css_selector(
            ".box-info-servico p")

        return [element.text for element in boxInfoServicoElement]

    def getProducts(self):
        productElements = self.driver.find_elements_by_css_selector(
            ".produtos_ajax")

        products = []

        for productElement in productElements:
            product = Product()
            try:
                product["url"] = productElement.find_element_by_css_selector(
                    ".btn-compra a.btn-success.tip").get_attribute("href")
            except NoSuchElementException:
                product["url"] = "ESGOTADO"

            product['price'] = productElement.find_element_by_id(
                "espSrvPreco").text
            product['amount'] = productElement.find_element_by_css_selector(
                "#espSrvPreco + b").text
            product['name'] = productElement.find_element_by_css_selector(
                "h5").text
            product['details'] = self.getDetails(productElement)
            products.append(product)

        return products

    def myParse(self):
        for link in self.linksCategories:
            self.driver.get(link)
            for product in self.getProducts():
                yield product

    def parse(self, response):
        for link in self.linksCategories:
            self.driver.get(link)
            for product in self.getProducts():
                yield product


if __name__ == '__main__':
    ZapgraficaSpider().myParse()
