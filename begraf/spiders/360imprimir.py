# -*- coding: utf-8 -*-
import scrapy
import os
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.select import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from begraf.items import Product


class EgraphSpider(scrapy.Spider):
    name = '360imprimir'
    allowed_domains = ['360imprimir.com.br']
    start_urls = ['https://www.360imprimir.com.br/']
    CHROMEDRIVER_PATH = os.path.join(os.path.dirname(__file__), "..","..","driver","chromedriver.exe")

    def __init__(self):
        self.setUpWebdriver()

    def setUpWebdriver(self):
        self.options = Options()
        self.options.add_argument("--window-size=1920,1080")
        self.options.add_argument("--start-maximized")
        self.options.add_argument("--headless")
        self.driver = webdriver.Chrome(
            options=self.options,
            executable_path=self.CHROMEDRIVER_PATH)
        self.driver.maximize_window()
        self.driver.get(self.start_urls[0])

    def getLinksCategories(self):
        menu_hover = self.driver.find_element_by_css_selector(
            ".col-table-2-12.nav-hover-blue"
        )
        hoverover = ActionChains(self.driver).move_to_element(
            menu_hover).perform()

        submenu_containing_categories = self.driver.find_elements_by_css_selector(
            ".navbar-submenu a")

        links = map(lambda link: link.get_attribute(
            "href"), submenu_containing_categories)
        return set(links)

    def getLinksSubCategories(self):
        pass

    def getDetails(self):

        try:
            details = []
            select = Select(self.driver.find_element_by_id("DimensionsType"))
            size = "Size: " + select.first_selected_option.text
            select = Select(self.driver.find_element_by_id("PapersType"))
            papers = "Paper: " + select.first_selected_option.text
            select = Select(self.driver.find_element_by_id("ColorsType"))
            colors = "Colors: " + select.first_selected_option.text
            select = Select(self.driver.find_element_by_id("FinishType"))
            finish = "Finish: " + select.first_selected_option.text
            details.append(size)
            details.append(papers)
            details.append(colors)
            details.append(finish)
            return details
        except NoSuchElementException:
            print("Some element not found")

    def getAmount(self):
        select = Select(self.driver.find_element_by_id("selectQuantity"))
        selectText = select.first_selected_option.get_property("innerText")

        return selectText

    def getProducts(self):
        priceElement = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(
                (By.CSS_SELECTOR, "#productPrice .price-result > div:last-child"))
        )

        self.product = Product()
        self.product["url"] = self.driver.current_url
        self.product['price'] = priceElement.get_property("innerText")
        self.product['amount'] = self.getAmount()
        self.product['name'] = self.driver.find_element_by_css_selector(
            ".page_envelope span:last-child span[itemprop='title']").text
        self.product['details'] = self.getDetails()

    def check_exists_by_css_selector(self, css_selector):
        try:
            self.driver.find_element_by_css_selector(css_selector)
        except NoSuchElementException:
            return False
        return True

    def parse(self, response):
        arr = [*self.getLinksCategories()]
        for link in arr[0:]:
            self.driver.get(link)
            if self.check_exists_by_css_selector(".quantity-panel"):
                self.getProducts()
                yield self.product
            else:
                continue
