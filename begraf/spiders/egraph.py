# -*- coding: utf-8 -*-

import re
import json
from urllib.parse import urljoin
from scrapy import Spider, Request
from scrapy.http import FormRequest

_ID_RE = re.compile(r"^(?:/[^/]+){2}/([^/]+)")
_QTY_ERROR_RE = re.compile(r"^\D+(\d+)\D+(\d+)")
_PRODUCT_DEADLINE_RE = re.compile(r"^\D+(\d+)")


class EgraphSpider(Spider):
    name = "egraph"
    allowed_domains = ("egraph.com.br",)
    start_urls = ("http://egraph.com.br",)

    def parse(self, res):
        for rel_url in res.css(
                "#menu-categoria .sub-menu>li>a::attr(href)").getall():
            subc_id = _ID_RE.match(rel_url).group(1)
            subc_rel_url = f"/loja/produtos-subcategorias-produtos/{subc_id}/1/"

            yield Request(res.urljoin(subc_rel_url), self.extract_products)

    def extract_products(self, res):
        for rel_url in res.xpath("//div/div/h3/a/@href").getall():
            p_id = _ID_RE.match(rel_url).group(1)

            req = Request(res.urljoin(rel_url), self.parse_product)
            req.meta['product_id'] = p_id

            yield req

    def parse_product(self, res):
        product = {'url': res.url}

        p_info = _product_info_from_name(res.selector)
        product['name'] = p_info['name']
        product['category'] = p_info['category']
        product['format'] = p_info['format']
        product['material'] = p_info['material']
        product['color'] = p_info['color']
        product['finish'] = p_info['finish']

        product['image-url'] = _product_image(res.selector, res.url)

        product['deadline'] = _product_deadline(res.selector)

        product['price'] = _product_price(res.selector)

        product['templates'] = _product_templates(res.selector, res.url)

        # TODO: adicionar imagem

        p_id = res.meta['product_id']
        req = FormRequest(
            url="http://egraph.com.br/loja/nav/produtos_detalhes_preco.asp",
            formdata={
                'v': "3",
                'idProduto': p_id,
                'numQtd': "9999999"
            },
            callback=self.parse_product_quantity)
        req.meta['product'] = product
        req.meta['product_batch_size'] = p_info['batch-size']
        yield req

    def parse_product_quantity(self, res):
        payload = json.loads(res.text)
        min_max = _QTY_ERROR_RE.match(payload['txtErro'])

        product = res.meta['product']

        product['quantity'] = {
            'batch-size': res.meta['product_batch_size'],
            'minimum-order': int(min_max.group(1)),
            'maximum-order': int(min_max.group(2))
        }

        yield product


def _product_info_from_name(selector):
    info = {}

    # Nome do produto
    info['name'] = selector.css(".min-descricao h1::text").get()

    fields = info['name'].split("-", 6)

    # Tamanho do lote/pacote
    info['batch-size'] = int(re.match(r"^(\d+)", fields[0]).group(1))

    # Categoria
    info['category'] = fields[1].strip()

    # Formato (dimensões)
    info['format'] = fields[2].strip()

    # Material utilizado na fabricação do produto
    info['material'] = fields[3].strip()

    # Esquema de cores usado na fabricação
    info['color'] = fields[4].strip()

    # Acabamento presente no produto
    info['finish'] = fields[5].strip()

    return info


def _product_image(selector, base_url):
    return urljoin(base_url, selector.css("#image-big img::attr(src)").get())


def _product_deadline(selector):
    text = "".join(selector.css(".min-descricao::text").getall()).strip()
    return int(_PRODUCT_DEADLINE_RE.match(text).group(1))


def _product_price(selector):
    text = "".join(selector.css(".big-preco::text").getall())
    return text.strip()


def _product_templates(selector, base_url):
    templates = []

    for a in selector.css(".download-gabarito a"):
        name_fields = a.css(".gabarito-name::text").get().split("-")

        templates.append({
            'application': name_fields[-1].strip(),
            'url': urljoin(base_url,
                           a.xpath("@href").get())
        })

    return templates
